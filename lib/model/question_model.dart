import 'dart:convert';
import 'package:http/http.dart' as http;

class Question {
  late int no;
  late String imgDir;
  late String audioDir;

  Question(this.no, this.imgDir, this.audioDir);

  factory Question.fromJson(dynamic json) {
    return Question(
        json['no'] as int, json['image'] as String, json['audio'] as String);
  }

  @override
  String toString() {
    return '{ $no, $imgDir,$audioDir  }';
  }
}

Future<List<Question>> fetchQuestions() async {
  // print("oke");
  final response = await http.get(Uri.parse(
      "https://thachln.github.io/toeic-data/ets/2016/1/p1/data.json"));

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    List<Question> list = [];
    for (final jsonObject in json.decode(response.body)) {
      // print(response.body);
      list.add(Question.fromJson(jsonObject));
      // print(list[list.length - 1].title);
    }
    return list;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load questions');
  }
}
