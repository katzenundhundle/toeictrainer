// import 'dart:async';
// import 'dart:html';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter/services.dart';
import '../model/question_model.dart';
import 'package:just_audio/just_audio.dart';

//import 'package:assets_audio_player/assets_audio_player.dart';

// import '../Validator/mixin_login_validator.dart';
// import '../bloc/bloc.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Toeic',
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Toeic Questions"),
        ),
        resizeToAvoidBottomInset: false,
        body: QuestionScreen(),
      ),
    );
  }
}

// class ImagesListScreen extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() {
//     return MessagesState();
//   }
// }

class QuestionScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return QuestionScreenState();
  }
}

class QuestionScreenState extends State<StatefulWidget> {
  late Future<List<Question>> futureQuestions;
  late AudioPlayer player;

  @override
  void dispose() {
    player.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    player = AudioPlayer();
    futureQuestions = fetchQuestions();
  }

  // final _biggerFont = const TextStyle(fontSize: 18.0);
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: futureQuestions,
        builder: (context, AsyncSnapshot<List<Question>> snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data!.length,
              itemBuilder: (context, index) {
                print(index);
                var title = snapshot.data![index].no.toString();
                var imgPath = snapshot.data![index].imgDir;
                var mp3Path = snapshot.data![index].audioDir;
                print(imgPath);
                return Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    margin: const EdgeInsets.all(20),
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            title: Text(
                              title,
                              textAlign: TextAlign.center,
                            ),
                          ),
                          SizedBox(
                            width: null,
                            height: 200,
                            child: Image.asset("lib/assets/" + imgPath),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              TextButton(
                                child: const Text('Listen'),
                                onPressed: () async {
                                  await player
                                      .setAsset('lib/assets/' + mp3Path);
                                  player.play();
                                },
                              ),
                              const SizedBox(width: 20),
                              TextButton(
                                child: const Text('Answer'),
                                onPressed: () {/* ... */},
                              ),
                              const SizedBox(width: 8),
                            ],
                          ),
                        ],
                      ),
                    ));
              },
              padding: const EdgeInsets.all(10),
            );
          } else {
            return Text("");
          }
        });
  }
}
