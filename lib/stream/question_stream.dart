import 'dart:convert';

import '../model/question_model.dart';
import 'package:http/http.dart' as http;

final List<Question> list = [];

class QuestionStream {
  Stream<List<Question>> getQuestion() async* {
    fetchQuestions();
    // print(list[index].title);
    list.forEach((element) {
      print(element);
    });
    yield list;
  }
}

Future<List<Question>> fetchQuestions() async {
  // print("oke");
  final response = await http.get(Uri.parse(
      "https://thachln.github.io/toeic-data/ets/2016/1/p1/data.json"));

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.

    for (final jsonObject in json.decode(response.body)) {
      // print(response.body);
      list.add(Question.fromJson(jsonObject));
      // print(list[list.length - 1].title);
    }
    return list;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load questions');
  }
}

void main(List<String> args) {
  QuestionStream questionStream = QuestionStream();
  questionStream.getQuestion().listen((event) {
    event.forEach((element) {
      print(element);
    });
  });
}
